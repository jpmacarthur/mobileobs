﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HH.Core
{
    public class ExportAttribute : Attribute
    {
        public string ColumnName { get; set; }
        public double Width { get; set; }
        public bool WrapText { get; set; }
        public ExcelColumnFormatEnum Format { get; set; }
        public int Position { get; set; }

        public ExportAttribute(int position)
        {
            Position = position;
        }
    }
}
