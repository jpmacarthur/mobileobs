﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HH.Core.Models
{
    public class FloorModel
    {
        public int FloorId { get; set; }
        public string Name { get; set; }
    }
}
