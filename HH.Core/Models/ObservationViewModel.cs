﻿using System;
using System.Collections.Generic;
using System.Text;
using HH.Core;
using HH.Core.Models;

namespace HHCore.Models
{
    public class ObservationViewModel
    {
        public IList<FloorModel> Floors { get; set; }
        [Export(1, ColumnName = "location", Format = ExcelColumnFormatEnum.String)]
        public FloorModel SelectedFloor { get; set; }
        public IList<HCWModel> HCWs { get; set; }
        [Export(2, ColumnName = "HCW", Format = ExcelColumnFormatEnum.String)]

        public HCWModel SelectedHCW { get; set; }
        public IList<OpportunityModel> Opportunities { get; set; }
        [Export(3, ColumnName = "opportunity")]

        public OpportunityModel SelectedOpportunity { get; set; }
        public IList<HandComplianceModel> HandHygieneCompliances { get; set; }
        [Export(6, ColumnName = "handHygieneCompliance")]

        public HandComplianceModel SelectedHHC { get; set; }
        [Export(0, ColumnName = "timestamp", Format = ExcelColumnFormatEnum.DateAndTime)]

        public DateTime SubmitDate { get; set; }
        [Export(5, ColumnName = "note", Format = ExcelColumnFormatEnum.String)]
        public string Note { get; set; }
        public string UserId { get; set; }
        [Export(0, ColumnName = "timestamp")]

        public bool? IsMaskCompliant { get; set; }
        [Export(8, ColumnName = "timestamp")]
        public bool? IsGownCompliant { get; set; }
        [Export(7, ColumnName = "timestamp")]
        public bool? IsGlovesCompliant { get; set; }
        [Export(9, ColumnName = "timestamp")]
        public bool? IsRespiratorCompliant { get; set; }
        [Export(10, ColumnName = "timestamp")]
        public bool? IsBrownBleachCompliant { get; set; }

        [Export(11, ColumnName = "timestamp", Format = ExcelColumnFormatEnum.String)]
        public string FacilityName { get; set; }
    }
}
