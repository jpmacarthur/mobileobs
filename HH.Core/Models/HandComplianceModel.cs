﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HH.Core.Models
{
    public class HandComplianceModel
    {
        public int ComplianceId { get; set; }
        public string Name { get; set; }
    }
}
