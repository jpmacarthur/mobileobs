﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HH.Core.Models
{
    public class ObservationModel
    {
        public IList<OpportunityModel> Opportunities { get; set; }
        public IList<FloorModel> Floors { get; set; }
        public IList<HCWModel> Hcws { get; set; }
        public IList<HandComplianceModel> HandCompliance { get; set; }

    }
}
