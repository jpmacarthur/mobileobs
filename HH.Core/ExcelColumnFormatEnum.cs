﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HH.Core
{
    public enum ExcelColumnFormatEnum
    {
        Default,
        String,
        DateAndTime,
        DateOnly,
        TimeOnly
    }
}
