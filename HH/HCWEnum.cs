//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HH
{
    using System;
    
    public enum HCWEnum : int
    {
        Physician = 1,
        Nurse = 2,
        Radiology = 3,
        PhysicalTherapy = 4,
        Other = 5,
        PCA = 6,
        APRN = 7,
        NursingStudent = 8,
        Volunteer = 9,
        ChildLife = 10,
        RespiratoryTherapy = 11,
        MedicalStudent = 12,
        EDTech = 13,
        EVS = 14,
        DietaryServices = 15,
        Nutritionist = 16,
        Student = 17,
        SocialWork = 18,
        Anesthesia = 19,
        Resident = 20,
        RespiratoryTherapyStudent = 21
    }
}
