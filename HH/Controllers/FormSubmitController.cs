﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HH.Core.Models;
using HH.Extensions;
using HH.Models;
using HHCore.Models;
using Microsoft.AspNet.Identity;

namespace HH.Controllers
{
    public class FormSubmitController : ApiController
    {

        pmacarth_HygieneEntities db = new pmacarth_HygieneEntities();

        [Route("api/Floors/")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetFloors()
        {
            var floors = db.Floors.Select(f => new FloorModel
            {
                FloorId = f.FloorId.ToInt(),
                Name = f.Name
            });

            return Ok(floors.ToList());
        }

        [Route("api/HCW/")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetHCWPositions()
        {
            var positions = db.HCWs.Select(f => new HCWModel()
            {
                HCWId = f.HCWId.ToInt(),
                Name = f.Name
            });

            return Ok(positions.ToList());
        }

        [Route("api/Opportunities/")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetOpportunities()
        {
            var positions = db.Opportunities.Select(f => new OpportunityModel()
            {
                OpportunityId = f.OpportunityId.ToInt(),
                Name = f.Name
            });

            return Ok(positions.ToList());
        }

        [Route("api/HHCT/")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetHygieneCompliance()
        {
            var positions = db.HandHygieneComplianceTypes.Select(f => new HandComplianceModel()
            {
                ComplianceId = f.HandHygieneComplianceTypeId.ToInt(),
                Name = f.Name
            });

            return Ok(positions.ToList());
        }

        [Route("api/AllLists/")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            var compliaces = db.HandHygieneComplianceTypes.Select(f => new HandComplianceModel()
            {
                ComplianceId = (int)(f.HandHygieneComplianceTypeId),
                Name = f.Name
            }).ToList();

            var floors = db.Floors.Select(f => new FloorModel
            {
                FloorId = (int)(f.FloorId),
                Name = f.Name
            }).ToList();


            var hcws = db.HCWs.Select(f => new HCWModel()
            {
                HCWId = (int)(f.HCWId),
                Name = f.Name
            }).ToList();

            var opportunites = db.Opportunities.Select(f => new OpportunityModel()
            {
                OpportunityId = (int)(f.OpportunityId),
                Name = f.Name
            }).ToList();

            return Ok(new ObservationModel()
            {
                HandCompliance = compliaces,
                Floors = floors,
                Hcws = hcws,
                Opportunities = opportunites
            });
        }

        [Route("api/SubmitObservation/")]
        [Authorize]
        [HttpPost]
        public IHttpActionResult SubmitObservation(ObservationViewModel model)
        {
            var floor = db.Floors.FirstOrDefault(f => (int)f.FloorId == model.SelectedFloor.FloorId);
            var HHC = db.HandHygieneComplianceTypes.FirstOrDefault(f => (int)f.HandHygieneComplianceTypeId == model.SelectedHHC.ComplianceId);
            var op = db.Opportunities.FirstOrDefault(f => (int)f.OpportunityId == model.SelectedOpportunity.OpportunityId);
            var hcw = db.HCWs.FirstOrDefault(f => (int)f.HCWId == model.SelectedHCW.HCWId);
            var user = db.AspNetUsers.FirstOrDefault(f => f.Id == model.UserId);


            var observation = new Observation()
            {
                DateSubmitted = model.SubmitDate,
                Floor = floor,
                HandHygieneComplianceType = HHC,
                Opportunity = op,
                HCW = hcw,
                User = user,
                FacilityName = model.FacilityName,
                IsGloveCompliant = model.IsGlovesCompliant,
                IsGownCompliant = model.IsGownCompliant,
                IsMaskCompliant = model.IsMaskCompliant,
                IsRespiratorCompliant = model.IsRespiratorCompliant,
                IsBrownBleachCompliant = model.IsBrownBleachCompliant,
                Note = model.Note
            };

            db.Observations.Add(observation);
            db.SaveChanges();

            return Ok();
        }

        [Route("FormSubmit/SubmitObservation")]
        [Authorize]
        [HttpPost]
        public IHttpActionResult SubmitObservationFromWeb(ObservationViewModel model)
        {
            var floor = db.Floors.FirstOrDefault(f => (int)f.FloorId == model.SelectedFloor.FloorId);
            var HHC = db.HandHygieneComplianceTypes.FirstOrDefault(f => (int)f.HandHygieneComplianceTypeId == model.SelectedHHC.ComplianceId);
            var op = db.Opportunities.FirstOrDefault(f => (int)f.OpportunityId == model.SelectedOpportunity.OpportunityId);
            var hcw = db.HCWs.FirstOrDefault(f => (int)f.HCWId == model.SelectedHCW.HCWId);
            var userId = HttpContext.Current.User.Identity.GetUserId();
            var user = db.AspNetUsers.FirstOrDefault(f => f.Id == userId);


            var observation = new Observation()
            {
                DateSubmitted = model.SubmitDate,
                Floor = floor,
                HandHygieneComplianceType = HHC,
                Opportunity = op,
                HCW = hcw,
                User = user,
                FacilityName = model.FacilityName,
                IsGloveCompliant = model.IsGlovesCompliant,
                IsGownCompliant = model.IsGownCompliant,
                IsMaskCompliant = model.IsMaskCompliant,
                IsRespiratorCompliant = model.IsRespiratorCompliant,
                IsBrownBleachCompliant = model.IsBrownBleachCompliant,
                Note = model.Note
            };

            db.Observations.Add(observation);
            db.SaveChanges();

            return Ok();
        }

    }
}