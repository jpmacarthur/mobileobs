﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using HH.Core.Models;
using HH.Models;
using HHCore.Models;

namespace HH.Controllers
{
    public class ObservationController : ApiController 
    {
        pmacarth_HygieneEntities db = new pmacarth_HygieneEntities();

        [Route("Observations/GetByDateRange")]
        [HttpPost]
        public HttpResponseMessage GetFloors(ObservationRangeViewModel model)
        {
            var observations =
                db.Observations.Where(f => f.DateSubmitted > model.StartDate && f.DateSubmitted < model.EndDate).Select(obs=> new ObservationExcelModel()
                {
                    Floor = obs.Floor.Name,
                    HCW = obs.HCW.Name,
                    Opportunity = obs.Opportunity.Name,
                    HHC = obs.HandHygieneComplianceType.Name,
                    IsGlovesCompliant = obs.IsGloveCompliant,
                    IsBrownBleachCompliant = obs.IsBrownBleachCompliant,
                    IsGownCompliant = obs.IsGownCompliant,
                    IsMaskCompliant = obs.IsMaskCompliant,
                    IsRespiratorCompliant = obs.IsRespiratorCompliant,
                    FacilityName = obs.FacilityName,
                    SubmitDate = obs.DateSubmitted,
                    Note = obs.Note
        }).ToList();

            var stream = ExcelGeneration.ExcelGenerator.ExportToExcel(observations);
            var message = new HttpResponseMessage();
            message.Content = new StreamContent(stream);
            message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            message.Content.Headers.ContentDisposition.FileName = "Export.xlsx";
            message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            return message;
        }
    }
}