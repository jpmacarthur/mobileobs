﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HH.Core.Models;

namespace HH.Controllers
{
    public class UserInfoController : ApiController
    {
        pmacarth_HygieneEntities db = new pmacarth_HygieneEntities();

        [Route("api/UserInfo/{id}")]
        [Authorize]
        public IHttpActionResult GetUserInfo(string id)
        {
            var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            var user = db.AspNetUsers.Where(f=> f.Id == id).Select( u=> new UserInfoModel
            {
                DisplayName = u.DisplayName,
                FirstName = u.FirstName,
                LastName = u.LastName,
                UserName = u.UserName,
                UserId = u.Id,
                DailyCount = u.ObservationsForUser.Count(x=>x.DateSubmitted > today)
            }).FirstOrDefault();

            return Ok(user);
        }
    }
}