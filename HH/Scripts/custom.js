﻿$(document).ready(function () {
    $('.browncontact,#respCheckbox, .contact, .droplet').hide();

    $('#StartDate').datepicker({
        onSelect: function(dateText, inst) {
            $("input[name='StartDate']").val(dateText);
            var startDate = $.datepicker.parseDate("mm/dd/yy", $("input[name = 'StartDate']").val());
            $('#EndDate').datepicker("option", "minDate", startDate);
        }
    });

    $('#SubmitDateCal').datepicker({
        onSelect: function (dateText, inst) {
            $("input[name='SubmitDate']").val(dateText);
            var startDate = $.datepicker.parseDate("mm/dd/yy", $("input[name = 'SubmitDate']").val());
            $('#EndDate').datepicker("option", "minDate", startDate);
        }
    });

    $('#EndDate').datepicker({
        onSelect: function (dateText, inst) {
            $("input[name='EndDate']").val(dateText);
            var endDate = $.datepicker.parseDate("mm/dd/yy", $("input[name = 'EndDate']").val());
            $('#StartDate').datepicker("option", "maxDate", endDate);
        }
    });
    $("#IsolationType").change(function() {
        if ($("#IsolationType").val() === '1') {
            var closed = $(".contact:hidden");
            var open = $("[class=browncontact], [class=droplet]");

            $.each(closed,
                function(index, val) {
                    $(val).slideDown();
                });
            $.each(open,
                function(index, val) {
                    $(val).slideUp();
                });
        }
        if ($("#IsolationType").val() === '2') {
                var closed = $(".browncontact:hidden");
                var open = $("[class=droplet]");

                $.each(closed,
                    function(index, val) {
                        $(val).slideDown();
                    });
                $.each(open,
                    function(index, val) {
                        $(val).slideUp();
                    });
        }
        if ($("#IsolationType").val() === '3') {
            var closed = $(".droplet:hidden");
            var open = $("[class=browncontact]");

            $.each(closed,
                function (index, val) {
                    $(val).slideDown();
                });
            $.each(open,
                function (index, val) {
                    $(val).slideUp();
                });
        }
        if ($("#IsolationType").val() === '2') {
            var closed = $(".browncontact:hidden");
            var open = $("[class=droplet]");

            $.each(closed,
                function (index, val) {
                    $(val).slideDown();
                });
            $.each(open,
                function (index, val) {
                    $(val).slideUp();
                });
        }
        if ($("#IsolationType").val() === "") {
            $(".contact").slideUp();
            $(".droplet").slideUp();
            $(".browncontact").slideUp();

        }
    });
    $("#AirborneDrop").change(function () {
        if ($('#AirborneDrop option:selected').val() === '1') {
            $("#respCheckbox").slideToggle();
        } else {
            $("#respCheckbox").slideUp();
        }
    });
});