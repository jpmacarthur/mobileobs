﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HH.Core.Models;

namespace HH.Models
{
    public class ObservationEntryViewModel
    {
        public FloorEnum SelectedFloor { get; set; }
        public HCWEnum SelectedHCW { get; set; }
        public OpportunityEnum SelectedOpportunity { get; set; }
        public HandHygieneEnum SelectedHHC { get; set; }
        public DateTime SubmitDate { get; set; }
        public string Note { get; set; }
        public string UserId { get; set; }
        public bool? IsMaskCompliant;
        public bool? IsGownCompliant;
        public bool? IsGlovesCompliant;
        public bool? IsRespiratorCompliant;
        public bool? IsBrownBleachCompliant;
        public List<SelectListItem> IsolationTypes { get; set; }
        public List<SelectListItem> AirborneDropItems { get; set; }
        public string IsolationType { get; set; }

        public string FacilityName { get; set; }

        public ObservationEntryViewModel()
        {
            IsolationTypes = new List<SelectListItem>()
            {
                new SelectListItem()
                {
                    Value = "",
                    Text = "Select an option"
                },
                new SelectListItem()
                {
                    Value = "1",
                    Text = "Contact"
                },
                new SelectListItem()
                {
                    Value = "2",
                    Text = "Brown Contact"
                },
                new SelectListItem()
                {
                    Value = "3",
                    Text = "Droplet"
                }
            };
            AirborneDropItems = new List<SelectListItem>()
            {
                new SelectListItem()
                {
                    Value = "",
                    Text = "Select an option"
                },
                new SelectListItem()
                {
                    Value = "1",
                    Text = "Yes"
                },
                new SelectListItem()
                {
                    Value = "2",
                    Text = "No"
                }
            };
        }

        public ObservationEntryViewModel(ObservationEntryViewModel vm)
        {
            SelectedFloor = vm.SelectedFloor;
            SelectedHCW = vm.SelectedHCW;
            SelectedOpportunity = vm.SelectedOpportunity;
            SelectedHHC = vm.SelectedHHC;
            SubmitDate = vm.SubmitDate;
            UserId = vm.UserId;
            IsMaskCompliant = vm.IsMaskCompliant;
            IsGownCompliant = vm.IsGownCompliant;
            IsGlovesCompliant = vm.IsGlovesCompliant;
            IsRespiratorCompliant = vm.IsRespiratorCompliant;
            IsBrownBleachCompliant = vm.IsBrownBleachCompliant;
            FacilityName = vm.FacilityName;
            Note = Note;
            IsolationTypes = IsolationTypes;
        }
    }
}