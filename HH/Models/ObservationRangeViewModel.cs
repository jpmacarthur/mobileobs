﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HH.Models
{
    public class ObservationRangeViewModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}