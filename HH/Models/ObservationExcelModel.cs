﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages;
using HH.Core;
using HH.Core.Models;

namespace HH.Models
{
    public class ObservationExcelModel
    {
        [Export(1, ColumnName = "location", Format = ExcelColumnFormatEnum.String)]
        public string Floor { get; set; }
        [Export(2, ColumnName = "HCW", Format = ExcelColumnFormatEnum.String)]

        public string HCW { get; set; }
        [Export(3, ColumnName = "opportunity")]

        public string Opportunity { get; set; }
        [Export(6, ColumnName = "handHygieneCompliance")]

        public string HHC { get; set; }
        [Export(0, ColumnName = "timestamp", Format = ExcelColumnFormatEnum.DateAndTime)]

        public DateTime SubmitDate { get; set; }
        [Export(5, ColumnName = "note", Format = ExcelColumnFormatEnum.String)]
        public string Note { get; set; }
        [Export(9, ColumnName = "maskCompliant")]

        public bool? IsMaskCompliant { get; set; }
        [Export(8, ColumnName = "gownCompliant")]
        public bool? IsGownCompliant { get; set; }
        [Export(7, ColumnName = "gloveCompliant")]
        public bool? IsGlovesCompliant { get; set; }
        [Export(10, ColumnName = "respiratorCompliant")]
        public bool? IsRespiratorCompliant { get; set; }
        [Export(11, ColumnName = "brownBleachCompliant")]
        public bool? IsBrownBleachCompliant { get; set; }

        [Export(12, ColumnName = "facilityName", Format = ExcelColumnFormatEnum.String)]
        public string FacilityName { get; set; }
        public ObservationExcelModel() { }
        public ObservationExcelModel(Observation obs)
        {
            Floor = obs.Floor.Name;
            HCW = obs.HCW.Name;
            Opportunity = obs.Opportunity.Name;
            HHC = obs.HandHygieneComplianceType.Name;
            IsGlovesCompliant = obs.IsGloveCompliant;
            IsBrownBleachCompliant = obs.IsBrownBleachCompliant;
            IsGownCompliant = obs.IsGownCompliant;
            IsMaskCompliant = obs.IsMaskCompliant;
            IsRespiratorCompliant = obs.IsRespiratorCompliant;
            FacilityName = obs.FacilityName;
            SubmitDate = obs.DateSubmitted;
            Note = Note;
        }
    }
}