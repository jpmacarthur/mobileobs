﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using ClosedXML.Excel;
using HH.Core;
using HH.Extensions;

namespace HH.ExcelGeneration
{
    public class ExcelGenerator
    {
        public static Stream ExportToExcel<T>(List<T> results, string sheetName = "Export")
        {
            var excelAttributes = GetExportAttributes<T>();
            var excelProperties = GetExportProperties<T>();

            Stream outputStream = new MemoryStream();

            using (var workbook = new XLWorkbook())
            {
                var sheet = workbook.AddWorksheet(sheetName);

                for (var c = 0; c < excelAttributes.Count; c++)
                {
                    if (excelAttributes[c].Width > 0D)
                    {
                        sheet.Column(c + 1).Width = excelAttributes[c].Width;
                    }
                    else
                    {
                        sheet.Column(c + 1).AdjustToContents();
                    }
                    var headerRow = sheet.Row(1);
                    headerRow.Cell(c + 1).Value = excelAttributes[c].ColumnName;

                    headerRow.Style.Alignment.SetWrapText();
                    headerRow.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    headerRow.Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);
                }

                for (var r = 0; r < results.Count; r++)
                {
                    for (var c = 0; c < excelProperties.Count; c++)
                    {
                        var value = excelProperties[c].GetValue(results[r]);
                        var cell = sheet.Row(r + 2).Cell(c + 1);

                        SetCellValue(cell, value);

                        if (excelAttributes[c].WrapText)
                            cell.Style.Alignment.SetWrapText();

                        SetCellFormat(cell, value, excelAttributes[c]);
                    }
                }

                sheet.Columns().AdjustToContents();

                var range = sheet.Range(1, 1, results.Count + 1, excelProperties.Count);

                var table = range.CreateTable();
                
                workbook.SaveAs(outputStream);
            }
            outputStream.Flush();
            outputStream.Position = 0;

            return outputStream;
        }

        private static void SetCellFormat(IXLCell cell, object value, ExportAttribute attribute)
        {
            if (value is DateTime)
            {
                switch (attribute.Format)
                {
                    case ExcelColumnFormatEnum.Default:
                    case ExcelColumnFormatEnum.DateAndTime:
                        cell.Style.DateFormat.Format = @"[$-409]m/d/yyyy\ h:mm\ AM/PM;@";
                        break;
                    case ExcelColumnFormatEnum.DateOnly:
                        cell.Style.DateFormat.Format = @"[$-409]m/d/yyyy;@";
                        break;
                    case ExcelColumnFormatEnum.TimeOnly:
                        cell.Style.DateFormat.Format = @"[$-409]h:mm\ AM/PM;@";
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();

                }
            }

            if (attribute.Format == ExcelColumnFormatEnum.String)
                cell.DataType = XLDataType.Text;
        }

        private static void SetCellValue(IXLCell cell, object value)
        {
            if (value == null)
            {
                cell.Value = "N/A";
                return;
            }

            var type = value.GetType();

            if (value is bool)
            {
                cell.Value = (bool) value ? "YES" : "NO";
            }
            else if (type.IsEnum)
            {
                cell.Value = ((Enum) value).GetDescription();
            }
            else
            {
                cell.Value = value;
            }
        }

        public static List<PropertyInfo> GetExportProperties<T>()
        {
            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(f => f.GetCustomAttribute<ExportAttribute>(false) != null)
                .OrderBy(x => x.GetCustomAttribute<ExportAttribute>(false).Position).ToList();
            return props;
        }

        public static List<ExportAttribute> GetExportAttributes<T>()
        {
            var props = GetExportProperties<T>();

            var attributes = new List<ExportAttribute>();
            foreach (var propertyInfo in props)
            {
                var excelAttribute = propertyInfo.GetCustomAttribute<ExportAttribute>(false);

                if (string.IsNullOrEmpty(excelAttribute.ColumnName))
                {
                    excelAttribute.ColumnName = propertyInfo.Name;
                }
                
                attributes.Add(excelAttribute);
            }

            return attributes;
        }
    }
}