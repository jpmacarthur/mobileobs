﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using HHMobile.ViewModels;
using Newtonsoft.Json;

namespace HHMobile
{
    public static class ContextExtensions
    {
        public static ObservationViewModel ToModel(this object obj)
        {
            return (ObservationViewModel)obj;
        }

        public async static void Store(this ObservationViewModel vm)
        {
            if (string.IsNullOrEmpty(Settings.Settings.StoredObservations))
            {
                var list = new List<ObservationViewModel>();
                list.Add(vm);
                Settings.Settings.StoredObservations = JsonConvert.SerializeObject(list);
            }
            else
            {
                var obs = JsonConvert.DeserializeObject<List<ObservationViewModel>>(
                    Settings.Settings.StoredObservations);
                obs.Add(vm);
                Settings.Settings.StoredObservations = JsonConvert.SerializeObject(obs);
            }
        }

        public static List<T> ToObject<T>(this string obj)
        {
            try
            {
                return JsonConvert.DeserializeObject<List<T>>(obj);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
