﻿using System;
using HHMobile.Models;
using HHMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HHMobile.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
	    public LoginPage()
	    {
	        var vm = new LoginViewModel();
	        this.BindingContext = vm;
	        vm.DisplayInvalidLoginPrompt += () => DisplayAlert("Error", "Invalid Login, try again", "OK");
	        InitializeComponent();

	        Email.Completed += (object sender, EventArgs e) =>
	        {
	            Password.Focus();
	        };

	        Password.Completed += (object sender, EventArgs e) =>
	        {
	            try
	            {
	                vm.SubmitCommand.Execute(null);
	                //if (!string.IsNullOrEmpty(Settings.Settings.BearerToken))
	                //{
	                //    var rootPage = new NavigationPage(new MainPage());
	                //    Application.Current.MainPage = rootPage;
	                //}
	            }
	            catch
	            {
	                DependencyService.Get<IToast>().LongAlert("Error contacting the server. Check your network settings.");
	            }

            };
	    }
	}
}