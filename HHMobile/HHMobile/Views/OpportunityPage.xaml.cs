﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HH.Core.Models;
using HHMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HHMobile.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OpportunityPage : ContentPage
	{
		public OpportunityPage (ObservationViewModel vm)
		{
		    vm.Opportunities = Settings.Settings.StoredOpportunities.ToObject<OpportunityModel>();
            BindingContext = vm;
			InitializeComponent ();
		}

	    private void Hcws_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        Navigation.PushAsync(new IsolationType(BindingContext.ToModel()));
        }
    }
}