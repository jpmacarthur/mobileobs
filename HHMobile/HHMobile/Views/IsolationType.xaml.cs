﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HHMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HHMobile.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class IsolationType : ContentPage
	{
		public IsolationType (ObservationViewModel vm)
		{
		    BindingContext = vm;
			InitializeComponent ();
		}

	    private void Compliances_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        Navigation.PushAsync(new HandCompliancePage((ObservationViewModel)BindingContext));
	    }
	}
}