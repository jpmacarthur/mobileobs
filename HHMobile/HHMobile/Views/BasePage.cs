﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HHMobile.Services;
using Xamarin.Forms;

namespace HHMobile.Views
{
	public class BasePage : ContentPage
	{
	    protected ApiServices _service
	    {
	        get
	        {
	            if (_service == null)
	            {
	                return new ApiServices();
	            }
	            else return _service;
	        }
	        set { _service = value; }
	    }

	    protected async Task PopToPage(int page)
	    {
	        for (var counter = 1; counter < page; counter++)
	        {
	            Navigation.RemovePage(Navigation.NavigationStack[Navigation.NavigationStack.Count - 2]);
	        }
	        await Navigation.PopAsync();
	    }
	}
}