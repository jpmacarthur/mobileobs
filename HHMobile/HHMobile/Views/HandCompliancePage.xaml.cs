﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.Widget;
using HH.Core.Models;
using HHMobile.Models;
using HHMobile.Services;
using HHMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Application = Android.App.Application;

namespace HHMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HandCompliancePage : BasePage
    {
        private ApiServices _apiServices = new ApiServices();
        public HandCompliancePage(ObservationViewModel vm)
        {
            vm.HandHygieneCompliances = Settings.Settings.StoredIsolationTypes.ToObject<HandComplianceModel>();
            BindingContext = vm;
            InitializeComponent();

        }


        private async void Button_OnClicked(object sender, EventArgs e)
        {
            var context = (ObservationViewModel)BindingContext;
            context.IsLoading = true;
            var observation = new ObservationViewModel(context, false);
            observation.Note = Notes.Text;
            observation.SubmitDate = DateTime.Now;
            observation.UserId = Settings.Settings.UserId;

            observation.FacilityName = "Connecticut Children's Medical Center";
            if (!contacttoggle.IsToggled && !browntoggle.IsToggled && !dropletToggle.IsToggled)
            {
                observation.IsGownCompliant = null;
                observation.IsGlovesCompliant = null;
            }

            if (!browntoggle.IsToggled)
            {
                observation.IsBrownBleachCompliant = null;
            }

            if (!airborneToggle.IsToggled)
            {
                observation.IsRespiratorCompliant = null;
            }

            if (!dropletToggle.IsToggled)
            {
                observation.IsMaskCompliant = null;
            }

            MessagingCenter.Subscribe<SubmitError>(this, "storeObs", x =>
            {
                Navigation.PopToRootAsync();
                MessagingCenter.Send(this, "sent", context);
            });
            try
            {
                var success = await _apiServices.SubmitObservation(observation);
                if (success)
                {
                    context.HasBeenPreviouslyCompleted = true;
                    DependencyService.Get<IToast>().LongAlert("Successfully submitted Observation.");
                    var test = Navigation.NavigationStack.Count;
                    //Navigation.RemovePage();
                    for (var counter = 1; counter < 3; counter++)
                    {
                        Navigation.RemovePage(Navigation.NavigationStack[Navigation.NavigationStack.Count - 2]);
                    }

                    await Navigation.PopToRootAsync();
                    MessagingCenter.Send(this, "sent", context);
                    context.IsLoading = false;
                }
                else
                {
                    await Navigation.PushModalAsync(new SubmitError(observation));
                    DependencyService.Get<IToast>().LongAlert("Error Submitting Observation to the server.");
                    context.IsLoading = false;
                }
            }
            catch(Exception ex)
            {
                await Navigation.PushModalAsync(new SubmitError(context));
                DependencyService.Get<IToast>().LongAlert("Error contacting the server.");
                context.IsLoading = false;
            }



        }

        private void Picker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            Toggles.IsVisible = true;
            SubmitButton.IsEnabled = true;
        }

        private void Contact_OnToggled(object sender, ToggledEventArgs e)
        {
            if (e.Value == true)
            {
                browntoggle.IsToggled = false;
                dropletToggle.IsToggled = false;
                ContactToggleGroup.IsVisible = true;

            }
            else
            {
                ContactToggleGroup.IsVisible = false;
            }
        }

        private void Droplet_OnToggled(object sender, ToggledEventArgs e)
        {
            if (e.Value)
            {
                airborneToggle.IsToggled = false;
                contacttoggle.IsToggled = false;
                browntoggle.IsToggled = false;
                DropletToggleGroup.IsVisible = true;
            }
            else
            {
                DropletToggleGroup.IsVisible = false;
            }
        }

        private void Airborne_OnToggled(object sender, ToggledEventArgs e)
        {
            if (e.Value)
            {
                dropletToggle.IsToggled = false;
                Respirator.IsVisible = true;
            }
            else
            {
                Respirator.IsVisible = false;
            }
        }

        private void BrownContact_OnToggled(object sender, ToggledEventArgs e)
        {
            if (e.Value == true)
            {
                dropletToggle.IsToggled = false;
                contacttoggle.IsToggled = false;
                BrownToggleGroup.IsVisible = true;
            }
            else
            {
                BrownToggleGroup.IsVisible = false;
            }

        }
    }
}