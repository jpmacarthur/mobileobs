﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HH.Core.Models;
using HHMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HHMobile.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HCWPage : ContentPage
	{
		public HCWPage (ObservationViewModel vm)
		{
		    if (BindingContext == null)
		    {
		        vm.HCWs = Settings.Settings.StoredHCWs.ToObject<HCWModel>();
		        BindingContext = vm;
            }
            InitializeComponent ();
        }

	    private void Hcws_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        Navigation.PushAsync(new OpportunityPage(BindingContext.ToModel()));
        }
    }
}