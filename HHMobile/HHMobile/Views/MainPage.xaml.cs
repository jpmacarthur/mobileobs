﻿using System;
using System.Collections.Generic;
using System.Linq;
using HH.Core.Models;
using HHMobile.Services;
using HHMobile.ViewModels;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HHMobile.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : ContentPage
	{
        ApiServices _apiServices = new ApiServices();
		public MainPage ()
		{
		    var user = _apiServices.GetUserInfo(Settings.Settings.UserId);
		    if (Settings.Settings.StoredObservations.ToObject<ObservationViewModel>() != null)
		    {
		        storedObs.IsVisible = true;
		    }

		    BindingContext = user;
            InitializeComponent();
		    if (user == null)
		    {
		        Application.Current.MainPage = new LoginPage();
		        Navigation.RemovePage(this);
		    }

		    if (Settings.Settings.StoredFloors == "" ||
		        Settings.Settings.StoredHCWs == "" ||
		        Settings.Settings.StoredOpportunities == "" ||
		        Settings.Settings.StoredIsolationTypes == "")
		    {
		        var model = _apiServices.GetAll();
		        var bindingModel = new ObservationViewModel();

                if (model != null)
                {
                    Settings.Settings.StoredFloors = JsonConvert.SerializeObject(model.Floors);
                    Settings.Settings.StoredHCWs = JsonConvert.SerializeObject(model.Hcws);
                    Settings.Settings.StoredIsolationTypes = JsonConvert.SerializeObject(model.HandCompliance);
                    Settings.Settings.StoredOpportunities = JsonConvert.SerializeObject(model.Opportunities);
                }
                else
                {
                    Application.Current.MainPage = new LoginPage();
                }
            }

            MessagingCenter.Subscribe<HandCompliancePage, ObservationViewModel>(this, "sent", (sender, arg) =>
            {
                var prevModel = arg;
                var bindingModel = new ObservationViewModel()
                {
                    SelectedFloor = prevModel.SelectedFloor,
                    HasBeenPreviouslyCompleted = prevModel.HasBeenPreviouslyCompleted
                };

                if (Settings.Settings.StoredFloors == "" ||
                    Settings.Settings.StoredHCWs == "" ||
                    Settings.Settings.StoredOpportunities == "" ||
                    Settings.Settings.StoredIsolationTypes == "")
                {
                    var model = _apiServices.GetAll();
                    if (model != null)
                    {
                        Settings.Settings.StoredFloors = JsonConvert.SerializeObject(model.Floors);
                        Settings.Settings.StoredHCWs = JsonConvert.SerializeObject(model.Hcws);
                        Settings.Settings.StoredIsolationTypes = JsonConvert.SerializeObject(model.HandCompliance);
                        Settings.Settings.StoredOpportunities = JsonConvert.SerializeObject(model.Opportunities);
                    }
                }
                bindingModel.IsGownCompliant = false;
                bindingModel.IsMaskCompliant = false;
                bindingModel.IsGlovesCompliant = false;
                bindingModel.IsRespiratorCompliant = false;
                bindingModel.IsBrownBleachCompliant = false;
                var hcwpage = new HCWPage(bindingModel);
                Navigation.PushAsync(hcwpage);
                if (bindingModel.HasBeenPreviouslyCompleted)
                {
                    Navigation.InsertPageBefore(new FloorPage(bindingModel), hcwpage);
                }
            });
        }

	    private void Button_OnClicked(object sender, EventArgs e)
	    {
	        Navigation.PushAsync(new FloorPage());
	    }

	    private async void MenuItem_OnClicked(object sender, EventArgs e)
	    {
	        ((UserInfoModel) BindingContext).IsLoading = true;
	        Settings.Settings.UserId = null;
	        Settings.Settings.BearerToken = null;
	        await _apiServices.LogOff();
	        App.IsUserLoggedIn = false;
	        Application.Current.MainPage = new LoginPage();
	        ((UserInfoModel)BindingContext).IsLoading = false;
        }

	    protected override async void OnAppearing()
	    {
	        if (Settings.Settings.StoredObservations.ToObject<ObservationViewModel>()?.Any() ?? false)
	        {
                storedObs.IsVisible = true;
	        }
	        else
	        {
	            storedObs.IsVisible = false;
            }
            if (CrossConnectivity.Current.IsConnected)
	        {
	            //TODO GEt this working so that you can use it when not connected to the internet
	            var user = _apiServices.GetUserInfo(Settings.Settings.UserId);
	            if (user == null)
	            {
	                Settings.Settings.UserId = null;
	                Settings.Settings.BearerToken = null;
	                await _apiServices.LogOff();
	                App.IsUserLoggedIn = false;
	                Application.Current.MainPage = new LoginPage();
	            }
	            BindingContext = user;
	        }
        }


	    private void ViewStoredObservation_OnClicked(object sender, EventArgs e)
	    {
	        Navigation.PushAsync(new StoredObservations());
	    }
	}
}