﻿using System;
using System.Collections.Generic;
using System.Text;
using HHMobile.Services;
using Xamarin.Forms;

namespace HHMobile.Views
{
    public class MainPageCS : ContentPage
    {
        public MainPageCS()
        {
            var toolbarItem = new ToolbarItem
            {
                Text = "Logout"
            };
            toolbarItem.Clicked += OnLogoutButtonClicked;
            ToolbarItems.Add(toolbarItem);

        }

        async void OnLogoutButtonClicked(object sender, EventArgs e)
        {
            App.IsUserLoggedIn = false;
            Navigation.InsertPageBefore(new LoginPageCS(), this);
            ApiServices service = new ApiServices();
            await service.LogOff();
            await Navigation.PopAsync();
        }
    }
}
