﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HHMobile.ViewModels;
using HHMobile.Views.Modals;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HHMobile.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StoredObservations : ContentPage
	{
		public StoredObservations ()
		{
		    var items = Settings.Settings.StoredObservations.ToObject<ObservationViewModel>();
			InitializeComponent ();
		    Observations.ItemsSource = new ObservableCollection<ObservationViewModel>(items);

        }

        private void Observation_OnSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        Navigation.PushModalAsync(new SelectedObservation((ObservationViewModel)e.SelectedItem));
	    }
	}
}