﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HHMobile.Models;
using HHMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HHMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SubmitError : BasePage
    {
        private ObservationViewModel vm;

        public SubmitError(ObservationViewModel vm)
        {
            this.vm = vm;
            InitializeComponent();
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var success = await _service.SubmitObservation(vm);
                if (success)
                {
                    vm.HasBeenPreviouslyCompleted = true;
                    DependencyService.Get<IToast>().LongAlert("Successfully submitted Observation.");
                    var test = Navigation.NavigationStack.Count;
                    for (var counter = 1; counter < 3; counter++)
                    {
                        Navigation.RemovePage(Navigation.NavigationStack[Navigation.NavigationStack.Count - 2]);
                    }

                    await Navigation.PopToRootAsync();
                    MessagingCenter.Send(this, "sent", vm);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SaveObs_OnClicked(object sender, EventArgs e)
        {
            vm.Store();
            Navigation.PopModalAsync();
            MessagingCenter.Send(this, "storeObs");
            MessagingCenter.Send(this, "sent", vm);
        }
    }
}