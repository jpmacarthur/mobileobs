﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HHMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HHMobile.Views.Modals
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SelectedObservation : ContentPage
	{
		public SelectedObservation (ObservationViewModel model)
		{
		    BindingContext = model;
			InitializeComponent ();
		}
	}
}