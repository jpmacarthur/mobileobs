﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HH.Core.Models;
using HHMobile.Services;
using HHMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HHMobile.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FloorPage : ContentPage
	{
		ObservationViewModel bindingModel = new ObservationViewModel();
        ApiServices _apiServices = new ApiServices();
        public FloorPage ()
		{
		    bindingModel.Floors = Settings.Settings.StoredFloors.ToObject<FloorModel>();
		    bindingModel.HCWs = Settings.Settings.StoredHCWs.ToObject<HCWModel>();
		    bindingModel.Opportunities = Settings.Settings.StoredOpportunities.ToObject<OpportunityModel>();
		    bindingModel.HandHygieneCompliances = Settings.Settings.StoredIsolationTypes.ToObject<HandComplianceModel>();
            bindingModel.IsGownCompliant = false;
		    bindingModel.IsMaskCompliant= false;
		    bindingModel.IsGlovesCompliant = false;
		    bindingModel.IsRespiratorCompliant = false;
		    bindingModel.IsBrownBleachCompliant = false;

            this.BindingContext = bindingModel;
			InitializeComponent ();
        }
	    public FloorPage(ObservationViewModel vm)
	    {
	        bindingModel.Floors = Settings.Settings.StoredFloors.ToObject<FloorModel>();
	        bindingModel.HCWs = Settings.Settings.StoredHCWs.ToObject<HCWModel>();
	        bindingModel.Opportunities = Settings.Settings.StoredOpportunities.ToObject<OpportunityModel>();
	        bindingModel.HandHygieneCompliances = Settings.Settings.StoredIsolationTypes.ToObject<HandComplianceModel>();
            bindingModel.IsGownCompliant = false;
	        bindingModel.IsMaskCompliant = false;
	        bindingModel.IsGlovesCompliant = false;
	        bindingModel.IsRespiratorCompliant = false;
	        bindingModel.IsBrownBleachCompliant = false;
	        bindingModel.SelectedFloor = vm.SelectedFloor;

	        this.BindingContext = bindingModel;
	        InitializeComponent();
	    }

        private void Hcws_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        Navigation.PushAsync(new HCWPage(BindingContext.ToModel()));
        }

	    protected override void OnAppearing()
	    {
	        ((ObservationViewModel) BindingContext).SelectedFloor = null;
	    }
    }
}