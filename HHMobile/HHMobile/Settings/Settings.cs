﻿using System;
using System.Collections.Generic;
using System.Text;
using HHMobile.ViewModels;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace HHMobile.Settings
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get { return CrossSettings.Current; }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;


        #endregion


        public static string GeneralSettings
        {
            get { return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault); }
            set { AppSettings.AddOrUpdateValue(SettingsKey, value); }
        }
        public static string ApiPath
        {
            get { return AppSettings.GetValueOrDefault(nameof(ApiPath), "http://pmacarthur.com/"); }
            set { AppSettings.AddOrUpdateValue(nameof(ApiPath), value); }
        }

        public static string BearerToken
        {
            get { return AppSettings.GetValueOrDefault(nameof(BearerToken), string.Empty); }
            set { AppSettings.AddOrUpdateValue(nameof(BearerToken), value); }
        }
        public static string UserId
        {
            get { return AppSettings.GetValueOrDefault(nameof(UserId), string.Empty); }
            set { AppSettings.AddOrUpdateValue(nameof(UserId), value); }
        }

        public static DateTime ExpireDate
        {
            get { return AppSettings.GetValueOrDefault(nameof(ExpireDate), DateTime.Now.Subtract(new TimeSpan(0,0,0,10))); }
            set { AppSettings.AddOrUpdateValue(nameof(ExpireDate), value); }
        }

        public static string StoredObservations
        {
            get { return AppSettings.GetValueOrDefault(nameof(StoredObservations), string.Empty); }
            set { AppSettings.AddOrUpdateValue(nameof(StoredObservations), value); }
        }

        public static string StoredHCWs
        {
            get { return AppSettings.GetValueOrDefault(nameof(StoredHCWs), string.Empty); }
            set { AppSettings.AddOrUpdateValue(nameof(StoredHCWs), value); }
        }
        public static string StoredIsolationTypes
        {
            get { return AppSettings.GetValueOrDefault(nameof(StoredIsolationTypes), string.Empty); }
            set { AppSettings.AddOrUpdateValue(nameof(StoredIsolationTypes), value); }
        }

        public static string StoredFloors
        {
            get { return AppSettings.GetValueOrDefault(nameof(StoredFloors), string.Empty); }
            set { AppSettings.AddOrUpdateValue(nameof(StoredFloors), value); }
        }

        public static string StoredOpportunities
        {
            get { return AppSettings.GetValueOrDefault(nameof(StoredOpportunities), string.Empty); }
            set { AppSettings.AddOrUpdateValue(nameof(StoredOpportunities), value); }
        }

    }
}
