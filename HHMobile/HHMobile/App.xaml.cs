﻿using System;
using HH.Core.Models;
using HHMobile.Services;
using Xamarin.Forms;
using HHMobile.Views;
using Newtonsoft.Json;
using Xamarin.Forms.Xaml;
using LoginPage = HHMobile.Views.LoginPage;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace HHMobile
{
	public partial class App : Application
	{
        private ApiServices _apiService = new ApiServices();
	    public static bool IsUserLoggedIn { get; set; }

        public App ()
        {
#if DEBUG
            Settings.Settings.ApiPath = "http://pmacarthur.com/";
            Settings.Settings.StoredObservations = "";
#else
            Settings.Settings.ApiPath = "http://pmacarthur.com/";
#endif
            InitializeComponent();
            if (Settings.Settings.ExpireDate > DateTime.Now && !string.IsNullOrEmpty(Settings.Settings.UserId) &&
                !string.IsNullOrEmpty(Settings.Settings.BearerToken))
            {
                IsUserLoggedIn = true;
            }

            var logintest = _apiService.GetUserInfo(Settings.Settings.UserId);
            if (logintest == null)
		    {
		        MainPage = new LoginPage();
		    }
		    else
		    {
		        MainPage = new NavigationPage(new HHMobile.Views.MainPage());
		    }
		}

		protected override void OnStart ()
		{

			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
		    var logintest = _apiService.GetUserInfo(Settings.Settings.UserId);
		    if (logintest == null)
		    {
		        MainPage = new LoginPage();
		    }

            if (IsUserLoggedIn && Settings.Settings.ExpireDate < DateTime.Now)
		    {
		        Settings.Settings.ExpireDate = DateTime.Now.AddSeconds(-1);
		        MainPage = new LoginPage();
            }
            // Handle when your app resumes
        }
	}
}
