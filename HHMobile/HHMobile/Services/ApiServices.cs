﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using HH.Core;
using HH.Core.Models;
using HHCore.Models;
using HHMobile.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HHMobile.Services
{
    public class ApiServices
    {
        public async Task RegisterUserAsync(
            string email, string password, string confirmPassword, string firstname, string lastname)
        {
            var model = new RegisterModel
            {
                Email = email,
                Password = password,
                ConfirmPassword = confirmPassword,
                FirstName = firstname,
                LastName = lastname
                
            };
            var json = JsonConvert.SerializeObject(model);
            HttpContent httpContent = new StringContent(json);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var client = new HttpClient();
            var response = await client.PostAsync(
                $"{Settings.Settings.ApiPath}/api/Account/Register", httpContent);
        }

        public async Task<string> LoginAsync(string userName, string password)
        {
            var keyValues = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("username", userName),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("grant_type", "password")
            };
            var content = new FormUrlEncodedContent(keyValues);
            var client = new HttpClient();
            var response = await client.PostAsync($"{Settings.Settings.ApiPath}/Token", content);
            var responsecontent = response.Content.ReadAsStringAsync();
            try
            {
                var tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(responsecontent.Result);
                var accessToken = tokenResponse.access_token;
                Settings.Settings.BearerToken = accessToken;
                Settings.Settings.UserId = tokenResponse.UserId;
                Settings.Settings.ExpireDate = DateTime.Now.AddSeconds(tokenResponse.expires_in - 5);

                return accessToken;
            }
            catch
            {
                return string.Empty;
            }
        }

        public async Task LogOff()
        {
            HttpClient client = new HttpClient();
            var response = await client.PostAsync($"{Settings.Settings.ApiPath}/Account/LogOff", new FormUrlEncodedContent(new Dictionary<string,string>()));
        }

        public UserInfoModel GetUserInfo(string id)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Settings.Settings.BearerToken);
            try
            {
                var json = client.GetStringAsync($"{Settings.Settings.ApiPath}/api/UserInfo/{id}").Result;
                var values = JsonConvert.DeserializeObject<UserInfoModel>(json);

                return values;
            }
            catch(Exception ex)
            {
                return null;
            }

        }

        public IList<FloorModel> GetFloors()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Settings.Settings.BearerToken);
            var json = client.GetStringAsync($"{Settings.Settings.ApiPath}/api/Floors/").Result;
            var values = JsonConvert.DeserializeObject<IList<FloorModel>>(json);

            return values;
        }

        public ObservationModel GetAll()
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", Settings.Settings.BearerToken);
                var json = client.GetAsync($"{Settings.Settings.ApiPath}/api/AllLists/");
                if (json.Result.IsSuccessStatusCode)
                {

                    var values =
                        JsonConvert.DeserializeObject<ObservationModel>(json.Result.Content.ReadAsStringAsync().Result);
                    return values;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public async Task<bool> SubmitObservation(HHMobile.ViewModels.ObservationViewModel model)
        {
            try
            {
                var json = JsonConvert.SerializeObject(model);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", Settings.Settings.BearerToken);
                var response = await client.PostAsync(
                    $"{Settings.Settings.ApiPath}/api/SubmitObservation", httpContent);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }

            return false;


        }
    }
}
