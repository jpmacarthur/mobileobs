﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using HHMobile.Models;
using HHMobile.Services;
using HHMobile.Views;
using Xamarin.Forms;

namespace HHMobile.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        public Action DisplayInvalidLoginPrompt;
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private bool isLoading;
        public bool IsLoading
        {
            get
            {
                return this.isLoading;
            }

            set
            {
                this.isLoading = value;
                RaisePropertyChanged("IsLoading");
            }
        }
        public void RaisePropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
        private string email;
        ApiServices _apiServices = new ApiServices();
        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Email"));
            }
        }
        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Password"));
            }
        }
        public ICommand SubmitCommand { protected set; get; }
        public LoginViewModel()
        {
            SubmitCommand = new Command(OnSubmit);
        }
        public async void OnSubmit()
        {
            IsLoading = true;
            string token = "";
            try
            {
                var test = Settings.Settings.ApiPath;
                token = await _apiServices.LoginAsync(email, password);

            }
            catch (HttpRequestException req)
            {
#if DEBUG
                DependencyService.Get<IToast>().LongAlert(req.Message);
#else
         DependencyService.Get<IToast>().LongAlert("Error: Could not contact server");
#endif
            }
            if (string.IsNullOrEmpty(token))
            {
                DisplayInvalidLoginPrompt();
            }
            else
            {
                Settings.Settings.BearerToken = token;
                if (!string.IsNullOrEmpty(Settings.Settings.BearerToken))
                {
                    var rootPage = new NavigationPage(new MainPage());
                    Application.Current.MainPage = rootPage;
                }
            }

            IsLoading = false;
        }
    }
}