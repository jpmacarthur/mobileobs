﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using HH.Core.Models;

namespace HHMobile.ViewModels
{
    public class ObservationViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public IList<FloorModel> Floors { get; set; }
        public FloorModel SelectedFloor { get; set; }
        public IList<HCWModel> HCWs { get; set; }
        public HCWModel SelectedHCW { get; set; }
        public IList<OpportunityModel> Opportunities { get; set; }
        public OpportunityModel SelectedOpportunity { get; set; }
        public IList<HandComplianceModel> HandHygieneCompliances { get; set; }
        public HandComplianceModel SelectedHHC { get; set; }
        public DateTime SubmitDate { get; set; }
        public bool HasBeenPreviouslyCompleted { get; set; }
        public string Note { get; set; }
        public string UserId { get; set; }
        private bool? isMaskCompliant;
        private bool? isGownCompliant;
        private bool? isGlovesCompliant;
        private bool? isRespiratorCompliant;
        private bool? isBrownBleachCompliant;

        public bool? IsMaskCompliant
        {
            get { return isMaskCompliant; }
            set
            {
                isMaskCompliant = value;
                PropertyChanged(this, new PropertyChangedEventArgs("IsMaskCompliant"));
            }
        }

        public bool? IsGownCompliant
        {
            get { return isGownCompliant; }
            set
            {
                isGownCompliant = value;
                PropertyChanged(this, new PropertyChangedEventArgs("IsGownCompliant"));
            }
        }

        public bool? IsGlovesCompliant
        {
            get { return isGlovesCompliant; }
            set
            {
                isGlovesCompliant = value;
                PropertyChanged(this, new PropertyChangedEventArgs("IsGlovesCompliant"));
            }
        }

        public bool? IsRespiratorCompliant
        {
            get { return isRespiratorCompliant; }
            set
            {
                isRespiratorCompliant = value;
                PropertyChanged(this, new PropertyChangedEventArgs("IsRespiratorCompliant"));
            }
        }

        public bool? IsBrownBleachCompliant
        {
            get { return isBrownBleachCompliant; }
            set
            {
                isBrownBleachCompliant = value;
                PropertyChanged(this, new PropertyChangedEventArgs("IsBrownBleachCompliant"));
            }
        }

        public string FacilityName { get; set; }
        public ObservationViewModel() { }
        public ObservationViewModel(ObservationViewModel vm, bool reset)
        {
            Floors = vm.Floors;
            SelectedFloor = vm.SelectedFloor;
            HCWs = vm.HCWs;
            SelectedHCW = vm.SelectedHCW;
            Opportunities = vm.Opportunities;
            SelectedOpportunity = vm.SelectedOpportunity;
            HandHygieneCompliances = vm.HandHygieneCompliances;
            SelectedHHC = vm.SelectedHHC;
            SubmitDate = vm.SubmitDate;
            UserId = vm.UserId;
            isMaskCompliant = vm.isMaskCompliant;
            isGownCompliant = vm.isGownCompliant;
            isGlovesCompliant = vm.isGlovesCompliant;
            isRespiratorCompliant = vm.isRespiratorCompliant;
            isBrownBleachCompliant = vm.isBrownBleachCompliant;
            FacilityName = vm.FacilityName;
            Note = Note;
            if (reset)
            {
                isMaskCompliant = null;
                isGownCompliant = null;
                isGlovesCompliant = null;
                isRespiratorCompliant = null;
                isBrownBleachCompliant = null;
                FacilityName = null;
                Note = null;
                SelectedOpportunity = null;
                SelectedHHC = null;
                SelectedHCW = null;
                SelectedFloor = null;
            }
        }

        private bool isLoading;
        public bool IsLoading
        {
            get
            {
                return this.isLoading;
            }

            set
            {
                this.isLoading = value;
                RaisePropertyChanged("IsLoading");
            }
        }

        public void RaisePropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

    }
}
