﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HHMobile.ViewModels
{
    public class UserDetailsViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string UserName { get; set; }
    }
}
