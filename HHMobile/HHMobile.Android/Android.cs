﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HHMobile.Droid;
using HHMobile.Models;
using Xamarin.Forms;
using Application = Android.App.Application;

[assembly:Dependency(typeof(Android_Toast))]
namespace HHMobile.Droid
{
    public class Android_Toast : IToast
    {
        public void LongAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();

        }
    }
}